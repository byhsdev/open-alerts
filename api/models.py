#
# This file is part of Open Alerts.
#
#    Open Alerts is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Open Alerts is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Open Alerts.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Open Alerts"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"

from django.db import models
from django.contrib.auth.models import User

class Device(models.Model):
    """Device info."""

    name = models.CharField(max_length=200)
    token = models.CharField(max_length=300)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class Event(models.Model):
    """Type of the event, e.g. Temperature, rat plague..."""

    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
    

class Action(models.Model):
    """Type of the action, e.g. Send email, play actuator..."""

    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class Notification(models.Model):
    """Custom configuration for one notification, defined by the user."""

    name = models.CharField(max_length=200, blank=True, null=True)
    event_type = models.ForeignKey(Event)
    event_args = models.CharField(max_length=300)
    action_type = models.ForeignKey(Action)
    action_args = models.CharField(max_length=500)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.event_args

    def __str__(self):
        return self.event_args

class TokenApi(models.Model):
    """Temporal token api."""

    token = models.CharField(max_length=200)
    expires = models.DateTimeField()
    user = models.ForeignKey(User)
