#
# This file is part of Open Alerts.
#
#    Open Alerts is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Open Alerts is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Open Alerts.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Open Alerts"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"

from django.conf.urls.defaults import *
from piston.resource import Resource
from myproject.api.handlers import *

login_handler = Resource(LoginHandler)
configure_alert_handler = Resource(ConfigureAlertHandler)
plague_info_handler = Resource(PlagueInfoHandler)

urlpatterns = patterns('',
    url(r'^1.0/(?P<username>\w+)/(?P<password>\w+)/login.(?P<emitter_format>.+)$', login_handler),
    url(r'^1.0/configure-alert.(?P<emitter_format>.+)$', configure_alert_handler),
    url(r'^1.0/plagues-info.(?P<emitter_format>.+)$', plague_info_handler),
)
