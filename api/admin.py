#
# This file is part of Open Alerts.
#
#    Open Alerts is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Open Alerts is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Open Alerts.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Open Alerts"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"

from django.contrib import admin
from myproject.api.models import *
from django.db.models import get_models, get_app

for model in get_models( get_app( 'api' ) ) :
    admin.site.register( model )
