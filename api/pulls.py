#!/usr/bin/python
# -*- coding: latin-1 -*-

#
# This file is part of Open Alerts.
#
#    Open Alerts is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Open Alerts is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Open Alerts.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Open Alerts"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"

import json
import requests
from django.conf import settings
from lxml import etree
from apns import APNs, Payload
from myproject.api.models import *
from django.conf import settings
import datetime

def send_iOS(message={'content':'No data'}, user=None):
    apns = APNs(use_sandbox=True, cert_file=settings.APN_CERT_FILE, key_file=settings.APN_KEY_FILE) 

    if not user:
        token_hex = Device.objects.get(name='iPhone').token
    token_hex = Device.objects.get(user=user).token
    payload = Payload(alert=json.dumps(message), sound="default", badge=1)
    print json.dumps(message)
    apns.gateway_server.send_notification(token_hex, payload)

allowed_attrs = ['TimeInstant', 'Latitud', 'Longitud', 'presence', 'batteryCharge', 'illuminance']

query_node = '<?xml version="1.0" encoding="UTF-8"?> \
<queryContextRequest> \
  <entityIdList> \
        <entityId type="Sensor" isPattern="true"> \
          <id>HACKSANTANDER.4IN1:86:DA:B6:0002</id> \
        </entityId> \
  </entityIdList> \
  <attributeList> \
  </attributeList> \
</queryContextRequest>'

query_subscribe_period = '<?xml version="1.0" encoding="UTF-8"?> \
<subscribeContextAvailabilityRequest> \
  <entityIdList> \
    <entityId type="Sensor" isPattern="true"> \
      <id>HACKSANTANDER.4IN1:86:DA:B6:0002</id> \
    </entityId> \
  </entityIdList> \
  <attributeList/> \
  <reference>http://130.206.83.32/period</reference> \
  <duration>PT3M</duration> \
</subscribeContextAvailabilityRequest>'

##########
# Response
##########
'''
<subscribeContextAvailabilityResponse>
  <subscriptionId>525ff6d81860a3f85a1b6235</subscriptionId>
  <duration>PT3M</duration>
</subscribeContextAvailabilityResponse>
'''

def subscribe_period():
    url = 'http://orion.lab.fi-ware.eu:1026/NGSI9/subscribeContextAvailability'
    headers = {'X-Auth-Token': settings.OAUTH_ACCESS_TOKEN, 'Content-Type': 'application/xml'}
    data = query_subscribe_period
    r = requests.post(url, data=data, headers=headers)
    print r.text

query_unsubscribe = '<?xml version="1.0" encoding="UTF-8"?> \
<unsubscribeContextAvailabilityRequest> \
  <subscriptionId>525ff6d81860a3f85a1b6235</subscriptionId> \
</unsubscribeContextAvailabilityRequest>'

def unsubscribe():
    url = 'http://orion.lab.fi-ware.eu:1026/NGSI9/unsubscribeContextAvailability'
    headers = {'X-Auth-Token': settings.OAUTH_ACCESS_TOKEN, 'Content-Type': 'application/xml'}
    data = query_unsubscribe
    r = requests.post(url, data=data, headers=headers)
    print r.text

######
# If some attribute changes, triggers a event. Throttling every 3min.
######
query_onchange_all = '<?xml version="1.0"?> \
<subscribeContextRequest> \
  <entityIdList> \
    <entityId type="Sensor" isPattern="true"> \
      <id>HACKSANTANDER.4IN1:86:DA:B6:0002</id> \
    </entityId> \
  </entityIdList> \
  <attributeList/> \
  <reference>http://130.206.83.32/period</reference> \
  <duration>P1M</duration> \
  <notifyConditions> \
    <notifyCondition> \
      <type>ONCHANGE</type> \
      <condValueList> \
        <condValue>pressure</condValue> \
        <condValue>temperature</condValue> \
        <condValue>illuminance</condValue> \
        <condValue>relativeHumidity</condValue> \
      </condValueList> \
    </notifyCondition> \
  </notifyConditions> \
  <throttling>PT3M</throttling> \
</subscribeContextRequest>' 

############
# Response
############
'''
<subscribeContextResponse>
  <subscribeResponse>
    <subscriptionId>525ffef31860a3eb3082d2dc</subscriptionId>
    <duration>P1M</duration>
    <throttling>PT3M</throttling>
  </subscribeResponse>
</subscribeContextResponse>
'''

def onchange_all():
    url = 'http://orion.lab.fi-ware.eu:1026/NGSI10/subscribeContext'
    headers = {'X-Auth-Token': settings.OAUTH_ACCESS_TOKEN, 'Content-Type': 'application/xml'}
    data = query_onchange_all
    r = requests.post(url, data=data, headers=headers)
    output = r.text

    print output   

#####
# Trigger event if 4in1 capture a movement, throttling every 10s.
#####
query_onchange_move = '<?xml version="1.0"?> \
<subscribeContextRequest> \
  <entityIdList> \
    <entityId type="Sensor" isPattern="true"> \
      <id>HACKSANTANDER.4IN1:86:DA:B6:0002</id> \
    </entityId> \
  </entityIdList> \
  <attributeList> \
    <attribute>Move</attribute> \
  </attributeList> \
  <reference>http://130.206.83.32/move</reference> \
  <duration>P1M</duration> \
  <notifyConditions> \
    <notifyCondition> \
      <type>ONCHANGE</type> \
      <condValueList> \
        <condValue>Move</condValue> \
      </condValueList> \
    </notifyCondition> \
  </notifyConditions> \
  <throttling>PT10S</throttling> \
</subscribeContextRequest>'  

######
# Response
######
'''
<subscribeContextResponse>
  <subscribeResponse>
    <subscriptionId>5260028b1860a37b30856050</subscriptionId>
    <duration>P1M</duration>
    <throttling>PT10S</throttling>
  </subscribeResponse>
</subscribeContextResponse>
'''

def onchange_move():
    url = 'http://orion.lab.fi-ware.eu:1026/NGSI10/subscribeContext'
    headers = {'X-Auth-Token': settings.OAUTH_ACCESS_TOKEN, 'Content-Type': 'application/xml'}
    data = query_onchange_move
    r = requests.post(url, data=data, headers=headers)
    output = r.text

    print output    


def nodes():
    url = 'http://orion.lab.fi-ware.eu:1026/NGSI10/queryContext'
    headers = {'X-Auth-Token': settings.OAUTH_ACCESS_TOKEN, 'Content-Type': 'application/xml'}
    data = query_node
    r = requests.post(url, data=data, headers=headers)
    output = r.text

    print output

    doc = etree.fromstring(output)
    message = {}
    message['time'] = datetime.datetime.today().ctime()
    message['type'] = 'sensor'

    for ce in doc.findall('.//contextElement'):
        id = ce.find('.//id').text
        value = None

        for ca in ce.findall('.//contextAttribute'):
            if (ca.find('name').text == 'temperature'):
                value = ca.find('contextValue').text
                message['temperature'] = value
            elif (ca.find('name').text == 'Move'):
                value = ca.find('contextValue').text
                message['Move'] = value
            elif (ca.find('name').text == 'relativeHumidity'):
                value = ca.find('contextValue').text
                message['relativeHumidity'] = value
            elif (ca.find('name').text == 'illuminance'):
                value = ca.find('contextValue').text
                message['illuminance'] = value
        
        
        #send_iOS(message=message)

def rat_plagues():
    print "rat plagues"

def timer():
    print "timer"

def check_values():
    #subscribe_period() - NOT USE!! -> For debug trace.
    #unsubscribe() - NOT USE!! -> For debug trace.
    #onchange_all() - DONE
    #onchange_move() - DONE
    #nodes() - Invoked by the cron
    rat_plagues()
    timer()
