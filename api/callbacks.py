#
# This file is part of Open Alerts.
#
#    Open Alerts is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Open Alerts is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Open Alerts.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Open Alerts"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"

from myproject.api.pulls import send_iOS
from piston.utils import rc
from django.views.decorators.csrf import csrf_exempt
from myproject.api.models import *
from lxml import etree
import datetime
import requests
import json
from django.core.mail import send_mail

@csrf_exempt
def led_strip(args):
        
    url = 'http://130.206.80.45:5371/m2m/v2/services/HACKSANTANDER/devices/RGBS:86:DA:B6:0003/command'
    headers = {'Content-Type': 'application/json'}

    query_rgb = {}
    query_rgb['commandML'] = "<paid:command name=\"SET\"><paid:cmdParam name=\"FreeText\"> <swe:Text><swe:value>FIZCOMMAND %s</swe:value></swe:Text></paid:cmdParam></paid:command>" % (args)

    r = requests.post(url, data=json.dumps(query_rgb), headers=headers)

@csrf_exempt
def period_all(request):
    if request.POST:
        doc = etree.fromstring(request.raw_post_data)
        
        message = {}
        message['time'] = datetime.datetime.today().ctime()
        message['type'] = 'sensor'
        
        for ce in doc.findall('.//contextElement'):
            id = ce.find('.//id').text
            value = None

            for ca in ce.findall('.//contextAttribute'):
                if (ca.find('name').text == 'temperature'):
                    value = ca.find('contextValue').text
                    message['temperature'] = value
                elif (ca.find('name').text == 'relativeHumidity'):
                    value = ca.find('contextValue').text
                    message['relativeHumidity'] = value
                elif (ca.find('name').text == 'illuminance'):
                    value = ca.find('contextValue').text
                    message['illuminance'] = value

        email = Action.objects.get(name='email')
        actuador = Action.objects.get(name='actuador')
        
        '''
        # Alerts:
        t = Event.objects.get(name='temperature')
        t_list = Notification.objects.filter(event_type=t)
        for n in t_list:
            if int(n.event_args) < int(message['temperature']) and n.action_type == actuador:
                args = '0-0-99'
                if n.action_args == 'on':
                    args = '99-0-0'
                led_strip(args)
            send_iOS(message=message, user=n.user)
        
        i = Event.objects.get(name='illuminance')
        i_list = Notification.objects.filter(event_type=i)
        for n in i_list:
            if int(n.event_args) < int(message['illuminance']) and n.action_type == actuador:
                args = '0-0-99'
                if n.action_args == 'on':
                    args = '99-0-0'
                led_strip(args)
            send_iOS(message=message, user=n.user)
        
        h = Event.objects.get(name='relativeHumidity')
        h_list = Notification.objects.filter(event_type=h)
        for n in h_list:
            if int(n.event_args) < int(message['relativeHumidity']) and n.action_type == actuador:
                args = '0-0-99'
                if n.action_args == 'on':
                    args = '99-0-0'
                led_strip(args)
            send_iOS(message=message, user=n.user)
        '''
        daniel = User.objects.get(username='daniel')
        antonio = User.objects.get(username='antonio')
        send_iOS(message=message, user=daniel)
        send_iOS(message=message, user=antonio)
        return rc.ALL_OK
    return rc.FORBIDDEN

@csrf_exempt
def move(request):
    if request.POST:
        doc = etree.fromstring(request.raw_post_data)
        
        message = {}
        message['time'] = datetime.datetime.today().ctime()
        message['type'] = 'sensor'
        
        for ce in doc.findall('.//contextElement'):
            id = ce.find('.//id').text
            value = None

            for ca in ce.findall('.//contextAttribute'):
                if (ca.find('name').text == 'Move'):
                    value = ca.find('contextValue').text
                    message['Move'] = value
         
        e = Event.objects.get(name='Move')
        alerts_list = Notification.objects.filter(event_type=e)
        email = Action.objects.get(name='email')
        actuador = Action.objects.get(name='actuador')

        for n in alerts_list:
            if n.action_type == email:
                send_mail('Movement sensor', 'Movement event detected!', 'tsptoni@gmail.com', ['tsptoni@gmail.com'])
            if n.action_type == actuador:
                args = '99-0-0'
                if n.action_args == 'on':
                    args = '0-0-99'
                led_strip(args)
            send_iOS(message=message, user=n.user)
        return rc.ALL_OK
    return rc.FORBIDDEN

