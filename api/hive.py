#!/usr/bin/env python
# -*- coding: latin-1 -*-

########
# Author: Antonio Sanchez Pineda
########


import sys

from hive_service import ThriftHive
from hive_service.ttypes import HiveServerException
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

def fetch_all(args=None):
    #results = {}
    print args

    try:
        transport = TSocket.TSocket('130.206.80.46', 10000)
        transport = TTransport.TBufferedTransport(transport)
        protocol = TBinaryProtocol.TBinaryProtocol(transport)

        client = ThriftHive.Client(protocol)
        transport.open()
        if 'all' in args:
            query = "select count(*) from malaga_plagues where neighbourhood like '%s'" % (args['neighbourhood'])
        else:
            query = "select count(*) from malaga_plagues where neighbourhood like '%s'" % (args['neighbourhood'])
            for n in args['types']:
                query += " and type = '%s'" % (n)
        client.execute(query)
        results = client.fetchAll()[0]
        transport.close()

    except Thrift.TException, tx:
        pass

    return results
