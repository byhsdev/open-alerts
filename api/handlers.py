#
# This file is part of Open Alerts.
#
#    Open Alerts is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Open Alerts is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Open Alerts.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Antonio Sanchez Pineda

__author__ = "Antonio Sanchez Pineda"
__copyright__ = "Copyright 2013, Open Alerts"
__credits__ = ["Antonio Sanchez Pineda"]
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Antonio Sanchez Pineda"
__email__ = "iam@sanchezantonio.com"
__status__ = "Development"

from piston.handler import BaseHandler
from piston.utils import rc, HttpStatusCode
from myproject.api.models import *
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from myproject.api.hive import *
import hashlib
import datetime
import json
from django.conf import settings

#Equivalencias metodos - peticiones
'''
A resource can be just a class, but usually you would want to define at least 1 of 4 methods:

read is called on GET requests, and should never modify data (idempotent.)

create is called on POST, and creates new objects, and should return them (or rc.CREATED.)

update is called on PUT, and should update an existing product and return them (or rc.ALL_OK.)

delete is called on DELETE, and should delete an existing object. Should not return anything, just rc.DELETED.
'''

# http://130.206.83.32/api/1.0/<username>/<password>/login.{format}
class LoginHandler(BaseHandler):
    allowed_methods = ('GET',)
    model = User

    def read(self, request, username, password):
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                TokenApi.objects.filter(user=user).delete()
                token = TokenApi()
                token.token = hashlib.sha1(datetime.datetime.today().ctime()).hexdigest()
                token.user = user
                token.expires = datetime.datetime.today() + datetime.timedelta(days=7)
                token.save()
                resp = {}
                resp['device'] = user.device_set.all()[0]
                resp['token'] = token
                resp['user'] = user                
                
                return resp

        return rc.FORBIDDEN


# http://130.206.83.32/api/1.0/configure-alert.{format}
class ConfigureAlertHandler(BaseHandler):
    allowed_methods = ('POST',)

    def create(self, request):
        raw = request.raw_post_data
        args = json.loads(raw)
        print args
        token = args['token']
        try:
            user = TokenApi.objects.get(token=token).user
            print user
            alert = Notification()
            alert.event_type = Event.objects.get(name=args['event_type'])
            alert.event_args = args['event_args']
            alert.action_type = Action.objects.get(name=args['action_type'])
            alert.action_args = args['action_args']
            alert.user = user
            alert.save()
            
            return alert
        except:
            return rc.FORBIDDEN


# http://130.206.83.32/api/1.0/plagues-info.{format}
class PlagueInfoHandler(BaseHandler):
    allowed_methods = ('POST',)

    def create(self, request):
        raw = request.raw_post_data
        args = json.loads(raw)
        print args
        token = args['token']
        try:
            user = TokenApi.objects.get(token=token).user
            print user
        except:
            rc.FORBIDDEN
        result = fetch_all(args)
        return json.dumps(result)
