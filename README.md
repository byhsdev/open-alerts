Open Alerts is a project developed in just 36 hours during the Fi- Ware hackathon held in Santander, 2013.

Winner of the third prize.

Open Alerts is the creation of alerts in an easy and intuitive, allowing citizens to customize their own environment. For example: "If it rains , send me an email and close the blinds".
First, the user chooses an event type: Rain, high temperature, motion detection, etc.
Second, choose the action to perform if the event occurs: Send email, do an action on an actuator (close blinds, turn on light, ...) and many more!
Finally, all information is stored on the server and the server, whenever it detects new events, analyzes whether a user has subscribed, and which if true, executes the action you have configured.

Furthermore, this whole project would not be possible without the Fi-Ware platform, which provides a lot of services essential to the proper functioning of the project.

The code has not changed its functionality or refactored after the hackathon, and we understand that although the quality of code is not excellent, it's interesting to see at what level it has managed to get on the circumstances in which it has developed.

Important read the license terms, which is distributed under GPL.

The project on the server side has been developed by Antonio Sánchez Pineda.
The iOS application , which consumes data through the API of the same project, has been developed by Daniel Lopez Pedrosa, but whose code is not hosted in this repository.

=== SPANISH ===

Open Alerts es un proyecto desarrollado en tan sólo 36 horas durante la Hackaton de Fi-Ware celebrada en Santander, 2013.

Ganador del tercer premio.

Open Alerts consiste en la creación de alertas de una forma fácil e intuitiva, permitiéndole al ciudadano personalizar su propio entorno. Por ejemplo: "Si llueve, envíame un email y cierra las persianas".
En primer lugar, el usuario elige un tipo de evento: Lluvia, temperatura superior a x grados, detección de movimientos, etc.
En segundo lugar, elige la acción a llevar a cabo en caso de que suceda el evento: Enviar email, hacer una acción sobre un actuador (cerrar persianas, encender luz,...) ¡y muchas más!
Finalmente, se guarda toda la información en el servidor y éste, cada vez que detecte nuevos eventos, analiza si algún usuario se ha subscrito, y en caso de cumplirse, ejecuta la acción que tuviera configurada.

Además, todo este proyecto no sería posible sin contar con la plataforma Fi-Ware, la cual proveé una gran cantidad de servicios indispensables para el correcto funcionamiento del proyecto.

El código no ha modificado su funcionalidad ni refactorizado una vez finalizada la Hackaton, ya que entendemos que aunque la calidad del código no sea excelente, es interesante ver a qué nivel del mismo se ha conseguido llegar en las circumstancias en las que se ha desarrollado.

Importante leer los términos de licencia, el cual se distribuye bajo GPL.

El proyecto en el lado del servidor ha sido desarrollado por Antonio Sánchez Pineda.
La aplicación iOS, la cual consume los datos a través de la API de este mismo proyecto, ha sido desarrollada por Daniel López Pedrosa, aunque cuyo código no se encuentra alojado en este repositorio.
